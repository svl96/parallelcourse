#include "mpi.h"
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#define INFO_SIZE 4
#define TAG 1


int main(int argc, char* argv[]) {

	int size, rank, N, generations, out_points, size_per_process;

	MPI_Status status;
	std::ofstream output("out.txt");
	int rc = MPI_Init(&argc, &argv);
	printf("rc %d \n", rc);
	if (rc != 0) {
	    std::cout << "Error starting MPI." << std::endl;
	    MPI_Abort(MPI_COMM_WORLD, rc);
	}
    // number of process
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	// process number
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    printf("start rank: %d, size: %d \n", rank, size);
	if (rank == 0) {
	    // Processor 0
	    if (argc < 2) {
	        std::cout << "Input file not specified" << std::endl;
	        MPI_Finalize();
	        return 0;
	    }

	    // read file
	    std::ifstream input_file(argv[1]);

	    if (!input_file) {
	        std::cout << "Error File Open" << std::endl;
	        MPI_Finalize();
            return 0;
	    }
	    // input file params
	    input_file >> N >> generations >> out_points;
	    printf("N %d  gen %d out %d", N, generations, out_points);
	    size_per_process = N/(size - 1);
	    // read from file
	    int matrix[N][N];
	    for (size_t i = 0; i < N; ++i) {
	        std::string tmp;
	        input_file >> tmp;
	        for (size_t j = 0; j < N; ++j) {
                matrix[i][j] = tmp[j] - '0';
	        }
	    }
	    input_file.close();
        printf("after read \n");
        int info[INFO_SIZE];
        // Send initial information
	    info[0] = N;
	    info[1] = size_per_process;
	    info[2] = generations;
	    info[3] = out_points;
	    printf("size per process %d \n", size_per_process);
	    for (size_t dest = 1; dest < size; ++dest) {
	        MPI_Send(&info, INFO_SIZE, MPI_INT, dest, TAG, MPI_COMM_WORLD);
	    }
        int zero_slice[size_per_process][N];
	    for (int receiver_id = 1; receiver_id < size; ++receiver_id) {
	        // for each process
	        for (int k = 0; k < size_per_process; ++k) {
	            for (int l = 0; l < N; ++l) {
	                zero_slice[k][l] = matrix[k + ((receiver_id - 1) * size_per_process)][l];
	            }
	        }
	        MPI_Send(&zero_slice, N * size_per_process, MPI_INT, receiver_id, TAG, MPI_COMM_WORLD);
	    }
        printf("after send \n");
    } else {

        int local_info[INFO_SIZE];
        int source = 0;
        MPI_Recv(&local_info, INFO_SIZE, MPI_INT, source, TAG, MPI_COMM_WORLD, &status); // receive info

        // get from source zero
        N = local_info[0];
        size_per_process = local_info[1];
        generations = local_info[2];
        out_points = local_info[3];

        int slice[size_per_process][N]; // slice for the board
        MPI_Recv(&slice, N * size_per_process, MPI_INT, source, TAG, MPI_COMM_WORLD, &status); // receive slice

        // extra info
        int to_up[N];
        int to_down[N];
        int from_up[N];
        int from_down[N];


        for (int gen = 1; gen <= generations; ++gen) { // for generations
            printf("rank %d, gen %d \n", rank, gen);

            if (rank != size - 1) {
                // for all except last
                for (int j = 0; j < N; ++j) {
                    to_down[j] = slice[size_per_process - 1][j];
                }
                // send to down info about slice
                MPI_Send(&to_down, N, MPI_INT, rank + 1, TAG, MPI_COMM_WORLD);
            }

            if (rank != size - 1) {
                // for all except last recv
                MPI_Recv(&from_down, N, MPI_INT, rank + 1, TAG, MPI_COMM_WORLD, &status);
            } else {
                // for last
                std::fill(from_down, from_down + N, 0);
            }

            if (rank != 1) {
                // for all except first
                for (int j = 0; j < N; ++j) {
                    to_up[j] = slice[0][j];
                }
                MPI_Send(&to_up, N, MPI_INT, rank - 1, TAG, MPI_COMM_WORLD);
            }

            if (rank != 1) {
                // for all except first
                MPI_Recv(&from_up, N, MPI_INT, rank - 1, TAG, MPI_COMM_WORLD, &status);

            } else {
                // for first
                std::fill(from_up, from_up + N, 0);
            }


            // Count neighbours
            int sum = 0;
            int next_slice[size_per_process][N];
            for (int x = 0; x < size_per_process; ++x) {
                for (int y = 0; y < N; ++y) {
                    if (x == 0 && y == 0) { // upper-left cell
                        sum = slice[x + 1][y] + slice[x + 1][y + 1] + slice[x][y + 1] + from_up[0] + from_up[1];
                    } else if (x == 0 && y == N - 1) { // upper-right cell
                        sum = slice[x][y - 1] + slice[x + 1][y - 1] + slice[x + 1][y] + from_up[N - 2] + from_up[N - 1];
                    } else if (x == size_per_process - 1 && y == 0) { // lower-left cell
                        sum = slice[x][y + 1] + slice[x - 1][y + 1] + slice[x - 1][y] + from_down[0] + from_down[1];
                    } else if (x == size_per_process - 1 && y == N - 1) { // lower-right cell
                        sum = slice[x - 1][y] + slice[x - 1][y - 1] + slice[x][y - 1] + from_down[N - 1] +
                              from_down[N - 2];
                    } else { // not corner cell

                        if (y == 0) { // left line
                            sum = slice[x - 1][y] + slice[x + 1][y] +
                                  slice[x - 1][y + 1] + slice[x][y + 1] + slice[x + 1][y + 1];
                        } else if (y == N - 1) {  // right line
                            sum = slice[x - 1][y] + slice[x + 1][y] +
                                  slice[x - 1][y - 1] + slice[x][y - 1] + slice[x + 1][y - 1];
                        } else if (x == 0) { // upper line
                            sum = slice[x][y - 1] + slice[x][y + 1] +
                                  slice[x + 1][y - 1] + slice[x + 1][y] + slice[x + 1][y + 1] +
                                  from_up[y - 1] + from_up[y] + from_up[y + 1];

                        } else if (x == size_per_process - 1) { // lower line
                            sum = slice[x][y + 1] + slice[x][y - 1] +
                                  slice[x - 1][y - 1] + slice[x - 1][y] + slice[x - 1][y + 1] +
                                  from_down[y - 1] + from_down[y] + from_down[y + 1];
                        } else {
                            sum = slice[x - 1][y - 1] + slice[x - 1][y] + slice[x - 1][y + 1] +
                                  slice[x][y + 1] + slice[x][y - 1] +
                                  slice[x + 1][y - 1] + slice[x + 1][y] + slice[x + 1][y + 1];
                        }
                    }

                    // PUT new value to cell
                    if (slice[x][y] == 1 && sum > 3) {
                        next_slice[x][y] = 0;
                    } else if (slice[x][y] == 1 && sum >= 2) {
                        next_slice[x][y] = 1;
                    } else if (slice[x][y] == 1 && sum <= 1) {
                        next_slice[x][y] = 0;
                    } else if (slice[x][y] == 0 && sum == 3) {
                        next_slice[x][y] = 1;
                    } else {
                        next_slice[x][y] = 0;
                    }
                }
            }

            // copy
            for (int x = 0; x < size_per_process; ++x) {
                for (int y = 0; y < N; ++y) {
                    slice[x][y] = next_slice[x][y];
                }
            }

            // print result
            if (gen % out_points == 0) {
                printf("save \n");
                if (rank == 1) {
                    output << "Generation " << gen << ":" << std::endl;
                    for (int x = 0; x < size_per_process; ++x) {
                        for (int y = 0; y < N; ++y) {
                            output << slice[x][y];
                        }
                        output << std::endl;
                    }
                    int out_slice[size_per_process][N];
                    for (int source_ind = 2; source_ind < size; ++source_ind) {
                        MPI_Recv(&out_slice, N * size_per_process, MPI_INT, source_ind, TAG, MPI_COMM_WORLD, &status);
                        for (int x = 0; x < size_per_process; ++x) {
                            for (int y = 0; y < N; ++y) {
                                output << out_slice[x][y];
                            }
                            output << std::endl;
                        }
                    }
                    output << std::endl << std::endl;
                } else {
                    MPI_Send(&slice, N * size_per_process, MPI_INT, 1 /* dest */, TAG, MPI_COMM_WORLD);
                }
            }
        }
    }
	MPI_Finalize();
    output.close();

	return 0;
}



















#include <memory>

#include <vector>
#include <mutex>
#include <iostream>
#include <memory>
#include <thread>


class Fork {
public:
	std::mutex lock;
};


void Eat(Fork* left_fork, Fork* right_fork, int phil) {

    if (left_fork == right_fork) {
        return;
    }

    std::lock(left_fork->lock, right_fork->lock);
    std::lock_guard<std::mutex> left(left_fork->lock, std::adopt_lock);
    std::lock_guard<std::mutex> right(right_fork->lock, std::adopt_lock);

    std::chrono::milliseconds timeout(500);
    std::this_thread::sleep_for(timeout);
    printf("Philosopher %d eats\n", phil);
}


int main(int argc, char const *argv[]) {
    int phil_count;
    std::cin >> phil_count;

    std::vector<std::unique_ptr<Fork>> forks;
    forks.reserve(phil_count);
    for (int ind = 0; ind < phil_count; ++ind) {
        forks.push_back(std::make_unique<Fork>());
    }

    std::vector<std::thread> tasks;
    tasks.reserve(phil_count);
    for (int i = 0; i < phil_count; ++i) {
        tasks.emplace_back(Eat,
                forks[i].get(), forks[(i + 1) % phil_count].get(), i);
    }

    for (auto& task : tasks) {
        task.join();
    }

	return 0;
}

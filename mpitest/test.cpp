#include <mpi.h>
#include <iostream>

int main(int argc, char** argv) {
	int rank, size, len;
	char hostname[24];
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Get_processor_name(hostname, &len);

	printf("Hello, MPI world! I am %d of %d on %s\n", rank, size, hostname);

	MPI_Finalize();
}
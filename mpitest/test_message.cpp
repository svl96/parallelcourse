#include <mpi.h>
#include <iostream>

int main(int argc, char** argv) {
	int rank, size, len, tag=1;
	char hostname[24];
	char msg[50];
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Get_processor_name(hostname, &len);

	if (rank == 0) {
		int i;
		for (int i = 0; i < size; ++i) {
			printf("Hello, from %d to %d", rank, i);
			MPI_Send(msg, 50, MPI_CHARACTER, i, tag, MPI_COMM_WORLD);
		}
	}
	MPI_Status status;
	MPI_Recv(msg, 50, MPI_CHARACTER, 0, tag, MPI_COMM_WORLD, &status);
	printf("Msg from %d: '%s'\n", status.MPI_SOURCE, msg);
	
	MPI_Finalize();
}